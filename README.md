# Vertlin

A minimalistic Vertx / Kotlin overcoat library that aims to speed up applications development.

## Introduction

This library attempts to provide utilities to enhance performance as well as speeding up the code writing process.
It prefers the exposure of Kotlin extensions function rather that requiring the user to inherit from abstract classes.

## Features

### Configuration

Exposes an extension function that allows configuration retrieval directly from the Vertx object.
It fetches configuration values from environment variables as well as a properties file.
Properties file defaults to *src/main/resources/application.properties*

````kotlin

import io.vertx.core.Vertx

val configKeys = listOf("SERVER_PORT", "DB_CONNECTION_STRING")
val vertx = Vertx.vertx().retrieveConfiguration(configKeys, "application.properties")
    .onFailure { 
        // ...
    }
    .onSuccess { config // a vertx JsonObject
        val serverPort = config.getInt("SERVER_PORT")
        // ...
    }
````

### EventBus

Facilitates the use of the event bus and ensures type safety regarding input and output values.
Input and output values are serialized using *kotlin.serialization* for enhanced performance.
Thus, input and output types should be annotated with *@Serializable* from the *kotlinx.serialization* package

Event bus endpoints are separated in 2 categories :

- **Event Bus APIs** : Event Bus addresses that takes and input returns a response and should never be broadcasted. Very similar to an HTTP Api without the protocol and reflection based serialization overhead
- **Event Bus Events Topics** : Event Bus addresses that only takes an input and to which messages should be broadcast. It can be used for volatile events dispatch such as cache invalidation

````kotlin
import io.vertx.core.Vertx

@Serializable
data class User(val name: String)

@Serializable
data class CreateUserCommand(val name: String)

@Serializable
data class UserCreatedEvent(val user: User)

object EventBusApis {
    val USER_GET_BY_ID = EventBusApiEndpoint<String, User>("users.get-by-id")
    val USER_CREATE = EventBusApiEndpoint<CreateUserCommand, User>("users.create")
}

object EventBusEvents {
    val USER_CREATED = EventBusEventEndpoint<UserCreatedEvent>("users.user-created")
}

fun demo() {
    val vertx = Vertx.vertx()
    
    vertx.eventBus().eventHandler(EventBusEvents.USER_CREATED) { event
        println("A user have been created : ${event.user}")
    }
    
    vertx.eventBus().apiHandler(EventBusApis.USER_GET_ALL) { userName ->
        return@apiHandler Future.suceededFuture(User(userName))
    }
    
    vertx.eventBus().apiHandler(EventBusApis.USER_CREATE) { command ->
        // ...
        val user = User(command.name)
        val event = UserCreatedEvent(user)
        vertx.eventBus().broadcastEvent(EventBusEvents.USER_CREATED, event)
        return@apiHandler Future.suceededFuture(user)
    }

    vertx.eventBus().requestApi(EventBusApis.USER_CREATE, CreateUserCommand("John"))
    
}
````

### Exceptions

