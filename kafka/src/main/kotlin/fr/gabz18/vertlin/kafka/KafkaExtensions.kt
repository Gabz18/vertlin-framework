package fr.gabz18.vertlin.kafka

import fr.gabz18.vertlin.commons.*
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.impl.VertxInternal
import io.vertx.kafka.client.consumer.KafkaConsumer
import io.vertx.kafka.client.consumer.KafkaConsumerRecord
import io.vertx.kafka.client.producer.KafkaProducer
import io.vertx.kafka.client.producer.KafkaProducerRecord
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
abstract class KafkaEvent(
    val version: Int = 1
)

interface TopicAwareEvent {
    fun topic(): String
}

inline fun <reified T : KafkaEvent, U : TopicAwareEvent> KafkaProducer<String, String>.produce(
    topic: U,
    key: String,
    event: T
): Future<Unit> {
    val eventAsString = Json.encodeToString(event)
    val record = KafkaProducerRecord.create(topic.topic(), key, eventAsString)
    record.addHeader("correlationId", getOrGenerateCorrelationId())
    return this.send(record).mapEmpty()
}

abstract class KafkaEventHandler<T : KafkaEvent>(private val serializer: KSerializer<T>, protected val logger: Logger) {

    fun handle(eventAsString: String): Future<Unit> {
        return try {
            val event = JsonMapper.fromJsonString(serializer, eventAsString)
            this.handle(event)
        } catch (err: Exception) {
            logger.error(err) { "An error occurred while handling Kafka Record" }
            Future.failedFuture(err)
        }
    }

    abstract fun handle(event: T): Future<Unit>

}

class RetryableKafkaConsumer(
    vertx: Vertx,
    private val kafkaConsumer: KafkaConsumer<String, String>,
    private val producer: KafkaProducer<String, String>
) {

    private val vertx = vertx as VertxInternal

    private val logger = Logger.logger { }

    private var isListening = false

    private val handlersByTopic: MutableMap<String, KafkaEventHandler<KafkaEvent>> = mutableMapOf()

    private companion object {
        private const val RETRIES_COUNT_HEADER = "retries"
        private const val CORRELATION_ID_HEADER = "correlationId"
        private const val GROUP_ID_DEDICATED_HEADER = "groupId-dedicated"
        private const val MAX_RETRIES = 3
    }

    fun <E : KafkaEvent> topicHandler(topic: TopicAwareEvent, handler: KafkaEventHandler<E>): RetryableKafkaConsumer {
        if (isListening) {
            throw TechnicalException("Attempted to configure Kafka Consumer while already listening to topics")
        }
        handlersByTopic[topic.topic()] = handler as KafkaEventHandler<KafkaEvent>
        return this
    }

    fun listen(): Future<Void> {
        if (isListening) {
            throw TechnicalException("Attempted to start and already running Kafka Consumer")
        }
        isListening = true
        val topics = handlersByTopic.keys.toSet()

        kafkaConsumer
            .handler { record ->
                val correlationId = record.headers().find { it.key() == CORRELATION_ID_HEADER }?.value()?.toString()
                val context = if (vertx.context.isDuplicate) vertx.context else vertx.context.duplicate()
                context.exceptionHandler { err -> logger.error(err) { "An error occurred while handling record" } }
                context.runOnContext {
                    setCorrelationId(correlationId)
                    this.handleRecord(record)
                }
            }
            .exceptionHandler { err -> logger.error(err) { "An error occurred while handling record" } }

        return kafkaConsumer.subscribe(topics)
            .onSuccess { logger.info { "Subscribed to topics : $topics" } }
            .onFailure { err ->
                logger.error(err) { "An error occurred while starting consumer" }
                isListening = false
            }
    }

    fun close(): Future<Void> {
        isListening = false
        return kafkaConsumer.close()
            .onSuccess { logger.info { "Closed Kafka consumer" } }
            .onFailure { err ->
                isListening = true
                logger.error(err) { "An error occurred while closing consumer" }
            }
    }

    private fun handleRecord(record: KafkaConsumerRecord<String, String>) {
        val retryCount = record.headers().find { it.key() == RETRIES_COUNT_HEADER }?.value()?.getInt(0) ?: 0
        logger.info { "Received kafkaRecord(topic=${record.topic()}, partition=${record.partition()}, offset=${record.offset()}, retryCount=$retryCount, key=${record.key()}, value=${record.value()})" }
        val handler = handlersByTopic[record.topic()]
        if (handler == null) {
            logger.warn { "Received event without associated handler for topic=\"${record.topic()}\"" }
            return
        }
        handler.handle(record.value())
            .recover { err -> handleFailure(err, record, retryCount) }
    }

    private fun handleFailure(
        err: Throwable,
        record: KafkaConsumerRecord<String, String>,
        retryCount: Int
    ): Future<Unit> {
        logger.warn { "An error occurred while handling event : $err" }
        return if (retryCount < MAX_RETRIES) {
            logger.info { "Producing retry record" }
            this.sendRetryRecord(record.topic(), record.key(), record.value(), retryCount + 1)
        } else {
            this.sendToDeadLetterQueue(record.topic(), record.key(), record.value(), err)
        }
    }

    private fun sendRetryRecord(topic: String, key: String, value: String, attemptedExecutions: Int): Future<Unit> {
        val record = KafkaProducerRecord.create(topic, key, value)
        record.addHeader(RETRIES_COUNT_HEADER, Buffer.buffer().appendInt(attemptedExecutions))
        record.addHeader(CORRELATION_ID_HEADER, Buffer.buffer(getCorrelationId()))
        return producer.send(record)
            .onFailure { err -> logger.error(err) { "An error occurred while sending retry record" } }
            .onSuccess { logger.info { "Sent retry record" } }
            .mapEmpty()
    }

    private fun sendToDeadLetterQueue(topic: String, key: String, value: String, err: Throwable): Future<Unit> {
        TODO()
    }

}