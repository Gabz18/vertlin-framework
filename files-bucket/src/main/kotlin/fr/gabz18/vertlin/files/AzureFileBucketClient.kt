package fr.gabz18.vertlin.files

import com.azure.storage.blob.BlobContainerAsyncClient
import com.azure.storage.blob.models.ParallelTransferOptions
import fr.gabz18.vertlin.commons.InvalidDomainStateException
import fr.gabz18.vertlin.commons.Logger
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import reactor.core.publisher.Flux
import java.nio.ByteBuffer
import java.util.UUID

class AzureFileBucketClient(private val vertx: Vertx, private val client: BlobContainerAsyncClient) : FileBucketClient {

    private val logger = Logger.logger { }

    override fun uploadFile(initialFileName: String, fileContent: Buffer): Future<String> {
        val fileNameToUpload = "${UUID.randomUUID()}${extractFileNameExtension(initialFileName)}"

        val byteArray = fileContent.getBytes();
        val byteBuffer = ByteBuffer.wrap(byteArray)
        val byteBufferFlux = Flux.just(byteBuffer)
        val options = ParallelTransferOptions()

        val blobClient = client.getBlobAsyncClient(fileNameToUpload)
        return blobClient.upload(byteBufferFlux, options).toFuture(vertx)
            .onFailure { err -> logger.error(err) { "An error occurred while uploading $initialFileName" } }
            .onSuccess { logger.info { "Successfully uploaded $initialFileName as $fileNameToUpload" } }
            .map { fileNameToUpload }
    }

    override fun downloadFile(fileName: String): Future<Buffer> {
        val fileClient = client.getBlobAsyncClient(fileName)
        return fileClient.downloadStream().toFuture(vertx)
    }

    override fun deleteFile(fileName: String): Future<Unit> {
        val fileClient = client.getBlobAsyncClient(fileName)
        return fileClient.deleteIfExists().toFuture(vertx).mapEmpty()
    }

    private fun extractFileNameExtension(fileName: String): String {
        val extension = fileName.substring(fileName.lastIndexOf("."))
        if (extension.isEmpty()) {
            throw InvalidDomainStateException("Attempted to upload a file without an file name extension")
        }
        return extension
    }

}