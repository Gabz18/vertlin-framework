package fr.gabz18.vertlin.files

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.impl.VertxInternal
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.nio.ByteBuffer

internal fun <T> Mono<T>.toFuture(vertx: Vertx): Future<T?> {
    val sub = SingleOutputSubscriber<T>(vertx)
    this.subscribe(sub)
    return sub.future()
}

private class SingleOutputSubscriber<T>(vertx: Vertx) : Subscriber<T> {

    private val promise = (vertx as VertxInternal).promise<T?>()

    private var value: T? = null

    override fun onSubscribe(p0: Subscription) {
        p0.request(Long.MAX_VALUE)
    }

    override fun onError(p0: Throwable?) {
        promise.fail(p0)
    }

    override fun onComplete() {
        promise.complete(value)
    }

    override fun onNext(p0: T) {
        value = p0
    }

    fun future(): Future<T?> {
        return promise.future()
    }

}

internal fun Flux<ByteBuffer>.toFuture(vertx: Vertx): Future<Buffer> {
    val sub = ByteBufferResultSubscriber(vertx)
    this.subscribe(sub)
    return sub.future()
}

private class ByteBufferResultSubscriber(vertx: Vertx) : Subscriber<ByteBuffer> {

    private val promise = (vertx as VertxInternal).promise<Buffer>()

    private val buffer = Buffer.buffer()

    override fun onSubscribe(p0: Subscription) {
        p0.request(Long.MAX_VALUE)
    }

    override fun onError(p0: Throwable?) {
        promise.fail(p0)
    }

    override fun onComplete() {
        promise.complete(buffer)
    }

    override fun onNext(p0: ByteBuffer) {
        buffer.appendBytes(p0.array())
    }

    fun future() = promise.future()

}