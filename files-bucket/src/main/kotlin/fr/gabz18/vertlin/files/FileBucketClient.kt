package fr.gabz18.vertlin.files

import io.vertx.core.Future
import io.vertx.core.buffer.Buffer


interface FileBucketClient {

    fun uploadFile(initialFileName: String, fileContent: Buffer) : Future<String>

    fun downloadFile(fileName: String) : Future<Buffer>

    fun deleteFile(fileName: String) : Future<Unit>

}