package fr.gabz18.vertlin.eventbus

import fr.gabz18.vertlin.commons.*
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.eventbus.EventBus
import io.vertx.core.eventbus.Message
import io.vertx.core.impl.ContextInternal

abstract class EventBusRouter(protected val logger: Logger) {

    abstract fun mountRoutes(eventBus: EventBus)

    protected fun <I, O> EventBus.apiHandler(endpoint: EventBusApiEndpoint<I, O>, handler: (I) -> Future<O>) {
        consumer(endpoint.address) { message ->
            var context = Vertx.currentContext() as ContextInternal
            if (!context.isDuplicate) {
                context = context.duplicate()
            }
            context.runOnContext {
                unpackCorrelationId(message)
                logger.debug { "Received EventBus message at address \"${endpoint.address}\" : ${message.body()}" }
                try {
                    val input = JsonMapper.fromJsonString(endpoint.inputSerializer, message.body())
                    handler.invoke(input)
                        .onFailure { handleMessageHandlingException(message, it) }
                        .onSuccess {
                            val response = JsonMapper.toJsonString(endpoint.outputKSerializer, it)
                            message.reply(response)
                            logger.debug { "Successfully handled EventBus message : $response" }
                        }
                } catch (e: Exception) {
                    handleMessageHandlingException(message, e)
                }
            }
        }
    }

    private fun unpackCorrelationId(message: Message<String>) {
        val correlationId = message.headers().get(CORRELATION_ID)
        setCorrelationId(correlationId)
    }

    private fun handleMessageHandlingException(
        message: Message<String>,
        throwable: Throwable,
        withReply: Boolean = true
    ) {
        if (throwable is ApplicationException) {
            logger.info { "An ApplicationException occurred while handling message at \"${message.address()}\" : ${throwable.type} - ${throwable.message}" }
            if (withReply) {
                message.fail(throwable.type.code, throwable.message)
            }
        } else {
            logger.error(throwable) { "An unhandled exception occurred while handling message at \"${message.address()}\"" }
            if (withReply) {
                message.fail(ExceptionType.UNHANDLED.code, throwable.message)
            }
        }
    }
}