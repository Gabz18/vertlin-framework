package fr.gabz18.vertlin.eventbus

import fr.gabz18.vertlin.commons.*
import io.vertx.core.Future
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.EventBus
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.eventbus.ReplyFailure
import kotlinx.serialization.KSerializer

/**
 * Defines an eventbus API that aims to handle commands or queries and reply with a response
 */
data class EventBusApiEndpoint<I, O>(
    val address: String,
    val inputSerializer: KSerializer<I>,
    val outputKSerializer: KSerializer<O>
)

val logger = Logger.logger { }

const val CORRELATION_ID = "correlation-id"

fun EventBus.mountRouter(eventBusRouter: EventBusRouter) : EventBus {
    eventBusRouter.mountRoutes(this)
    return this
}

fun <I, O> EventBus.requestApi(
    endpoint: EventBusApiEndpoint<I, O>,
    input: I
): Future<O> {

    fun getDeliveryOptions(): DeliveryOptions {
        val correlationId = getCorrelationId()
        return if (correlationId != null) {
            DeliveryOptions().addHeader(CORRELATION_ID, correlationId)
        } else {
            DeliveryOptions()
        }
    }

    fun digestReplyFailure(throwable: Throwable): Throwable {
        return if (throwable is ReplyException) {
            if (throwable.failureType() == ReplyFailure.RECIPIENT_FAILURE) {
                ApplicationException(ExceptionType.fromInt(throwable.failureCode()), throwable.message)
            } else if (throwable.failureType() == ReplyFailure.NO_HANDLERS) {
                CommunicationException(ExceptionType.REMOTE_SERVICE_UNAVAILABLE, "Targeted service is unavailable")
            } else if (throwable.failureType() == ReplyFailure.TIMEOUT) {
                CommunicationException(ExceptionType.TIMEOUT, "Input processing resulted in a timeout")
            } else {
                CommunicationException(ExceptionType.UNHANDLED, "An unexpected failure occurred")
            }
        } else {
            throwable
        }
    }

    val jsonInput = JsonMapper.toJsonString(endpoint.inputSerializer, input)
    logger.debug { "Sending request at \"${endpoint.address}\" : $input" }
    return request<String>(endpoint.address, jsonInput, getDeliveryOptions())
        .otherwise {
            throw digestReplyFailure(it)
        }
        .map {
            try {
                val response = JsonMapper.fromJsonString(endpoint.outputKSerializer, it.body())
                logger.debug { "Received event bus response from : $response" }
                response
            } catch (e: Exception) {
                logger.error(e) { "An error occurred while deserializing response : ${it.body()}" }
                throw e
            }
        }
}