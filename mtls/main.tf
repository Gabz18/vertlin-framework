terraform {
  required_version = ">=0.12"

  required_providers {
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

locals {
  client-certificate-password = "password123"
}

resource "kubernetes_manifest" "bootstrap-issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Issuer"
    metadata   = {
      name      = "bootstrap-issuer"
      namespace = "default"
    }
    spec = {
      selfSigned = {

      }
    }
  }
}

resource "kubernetes_manifest" "root-certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata   = {
      name      = "root-certificate"
      namespace = "default"
    }
    spec = {
      isCA       = true
      secretName = "root-certificate"
      commonName = "Root Certificate"
      usages     = [
        "server auth",
        "client auth",
        "key encipherment",
        "digital signature"
      ]
      privateKey = {
        algorithm = "RSA"
        size      = 4096
        encoding  = "PKCS8"
      }
      issuerRef = {
        name = "bootstrap-issuer"
        kind = "Issuer"
      }
    }
  }
  depends_on = [
    kubernetes_manifest.bootstrap-issuer
  ]
}

resource "kubernetes_manifest" "root-issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Issuer"
    metadata   = {
      name      = "root-issuer"
      namespace = "default"
    }
    spec = {
      ca = {
        secretName = "root-certificate"
      }
    }
  }
  depends_on = [
    kubernetes_manifest.root-certificate
  ]
}

################# ZOOKEEPER #####################

resource "kubernetes_secret_v1" "zookeeper-keystore-password" {
  metadata {
    name      = "zookeeper-keystore-password"
    namespace = "default"
  }

  data = {
    password = base64encode(local.client-certificate-password)
  }

  type = "Opaque"
}

resource "kubernetes_manifest" "zookeeper-certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata   = {
      name      = "zookeeper-certificate"
      namespace = "default"
    }
    spec = {
      secretName = "zookeeper-certificate"
      commonName = "Zookeeper Certificate"
      privateKey = {
        algorithm = "RSA"
        size      = 4096
        encoding  = "PKCS8"
      }
      usages = [
        "server auth",
        "client auth",
        "key encipherment",
        "digital signature"
      ]
      issuerRef = {
        name = "root-issuer"
        kind = "Issuer"
      }
      keystores = {
        pkcs12 = {
          create            = true
          passwordSecretRef = {
            name = kubernetes_secret_v1.zookeeper-keystore-password.metadata.0.name
            key  = "password"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_secret_v1.zookeeper-keystore-password,
    kubernetes_manifest.bootstrap-issuer
  ]
}

resource "kubernetes_deployment_v1" "zookeeper" {
  metadata {
    name   = "zookeeper"
    labels = {
      app = "zookeeper"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "zookeeper"
      }
    }
    template {
      metadata {
        labels = {
          app = "zookeeper"
        }
      }
      spec {
        volume {
          name = "certs"
          secret {
            secret_name = "zookeeper-certificate"
          }
        }
        container {
          image = "zookeeper:3.8.0-temurin"
          name  = "zookeeper"
          volume_mount {
            mount_path = "/certs"
            name       = "certs"
          }
          env {
            name = "KEYSTORE_PASSWD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret_v1.zookeeper-keystore-password.metadata.0.name
                key  = "password"
              }
            }
          }
          env {
            name  = "JVMFLAGS"
            value = <<EOT
-Dzookeeper.serverCnxnFactory=org.apache.zookeeper.server.NettyServerCnxnFactory
-Dzookeeper.ssl.keyStore.location=/certs/keystore.p12
-Dzookeeper.ssl.keyStore.password=$KEYSTORE_PASSWD
-Dzookeeper.ssl.trustStore.location=/certs/truststore.p12
-Dzookeeper.ssl.trustStore.password=$KEYSTORE_PASSWD
EOT
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "zookeeper" {
  metadata {
    name = "zookeeper-service"
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.zookeeper.metadata.0.labels.app
    }
    port {
      port        = 2181
      target_port = 2181
    }

    type = "ClusterIP"
  }
}

################# ACTIVEMQ #####################

resource "kubernetes_deployment_v1" "activemq" {
  metadata {
    name   = "activemq"
    labels = {
      app = "activemq"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "activemq"
      }
    }
    template {
      metadata {
        labels = {
          app = "activemq"
        }
      }
      spec {
        container {
          image = "accountify-activemq:1.0"
          name  = "activemq"
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "activemq" {
  metadata {
    name = "activemq-service"
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.activemq.metadata.0.labels.app
    }
    port {
      port        = 5672
      target_port = 5672
    }

    type = "ClusterIP"
  }
}

################# KAFKA #####################

resource "helm_release" "kafka" {
  name = "kafka"

  set {
    name  = "replicaCount"
    value = 1
  }

  #set {
  #  name  = "persistence.storageClass"
  #  value = "managed-csi"
  #}

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "kafka"
}


###################### PARTNERS AS ##################

resource "kubernetes_secret_v1" "partners-as-keystore-password" {
  metadata {
    name      = "partners-as-keystore-password"
    namespace = "default"
  }

  data = {
    password = base64encode(local.client-certificate-password)
  }

  type = "Opaque"
}

resource "kubernetes_manifest" "partners-as-certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata   = {
      name      = "partners-as-certificate"
      namespace = "default"
    }
    spec = {
      secretName = "partners-as-certificate"
      commonName = "Partners AS Certificate"
      privateKey = {
        algorithm = "RSA"
        size      = 4096
        encoding  = "PKCS8"
      }
      usages = [
        "server auth",
        "client auth",
        "key encipherment",
        "digital signature"
      ]
      issuerRef = {
        name = "root-issuer"
        kind = "Issuer"
      }
      keystores = {
        pkcs12 = {
          create            = true
          passwordSecretRef = {
            name = kubernetes_secret_v1.partners-as-keystore-password.metadata.0.name
            key  = "password"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_secret_v1.partners-as-keystore-password,
    kubernetes_manifest.bootstrap-issuer
  ]
}

resource "kubernetes_deployment_v1" "partners-as" {
  metadata {
    name   = "partners-as"
    labels = {
      app = "partners-as"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "partners-as"
      }
    }
    template {
      metadata {
        labels = {
          app = "partners-as"
        }
      }
      spec {
        volume {
          name = "certs"
          secret {
            secret_name = "partners-as-certificate"
          }
        }
        container {
          image   = "partners-as:1.4"
          name    = "partners-as"
          command = ["java"]
          args    = ["-jar", "-Djavax.net.debug=ssl:handshake", "app.jar"]
          volume_mount {
            mount_path = "/certs"
            name       = "certs"
          }
          #### Mutual TLS ####
          env {
            name  = "ENABLE_EB_MTLS"
            value = "true"
          }
          env {
            name  = "EB_MTLS_KEYSTORE_PATH"
            value = "/certs/keystore.p12"
          }
          env {
            name  = "EB_MTLS_TRUSTSTORE_PATH"
            value = "/certs/truststore.p12"
          }
          env {
            name = "EB_MTLS_KEYSTORE_PSSWD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret_v1.partners-as-keystore-password.metadata.0.name
                key  = "password"
              }
            }
          }
          env {
            name  = "EB_MTLS_CERT_PATH"
            value = "/certs/tls.crt"
          }
          env {
            name  = "EB_MTLS_KEY_PATH"
            value = "/certs/tls.key"
          }
          env {
            name  = "EB_MTLS_CA_PATH"
            value = "/certs/ca.crt"
          }

          #### Application Config ####
          env {
            name  = "APP_ZOOKEEPER_HOSTS"
            value = "zookeeper-service"
          }
          env {
            name  = "APP_KAFKA_SERVERS"
            value = "kafka:9092"
          }
          env {
            name  = "APP_AMQP_HOSTNAME"
            value = "activemq-service"
          }
          env {
            name  = "APP_MONGO_URI"
            value = "mongodb://gruniqueaccount:X7sx9vaXW12u3a37HcWSCHKJvuvewrIpIttyqT45qDsdzfWZhEyCG0usO2ct0pDQIV0LfmdaLaUfACDbH4qcWw==@gruniqueaccount.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@gruniqueaccount@"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_service_v1.zookeeper,
    kubernetes_service_v1.activemq,
    helm_release.kafka,
    kubernetes_manifest.partners-as-certificate
  ]
}

###################### REST PORTAL ##################

resource "kubernetes_secret_v1" "rest-portal-keystore-password" {
  metadata {
    name      = "rest-portal-keystore-password"
    namespace = "default"
  }

  data = {
    password = base64encode(local.client-certificate-password)
  }

  type = "Opaque"
}

resource "kubernetes_manifest" "rest-portal-certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata   = {
      name      = "rest-portal-certificate"
      namespace = "default"
    }
    spec = {
      secretName = "rest-portal-certificate"
      commonName = "REST Portal Certificate"
      privateKey = {
        algorithm = "RSA"
        size      = 4096
        encoding  = "PKCS8"
      }
      usages = [
        "server auth",
        "client auth",
        "key encipherment",
        "digital signature"
      ]
      issuerRef = {
        name = "root-issuer"
        kind = "Issuer"
      }
      keystores = {
        pkcs12 = {
          create            = true
          passwordSecretRef = {
            name = kubernetes_secret_v1.rest-portal-keystore-password.metadata.0.name
            key  = "password"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_secret_v1.partners-as-keystore-password,
    kubernetes_manifest.bootstrap-issuer
  ]
}


resource "kubernetes_deployment_v1" "rest-portal" {
  metadata {
    name   = "rest-portal"
    labels = {
      app = "rest-portal"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "rest-portal"
      }
    }
    template {
      metadata {
        labels = {
          app = "rest-portal"
        }
      }
      spec {
        volume {
          name = "certs"
          secret {
            secret_name = "rest-portal-certificate"
          }
        }
        container {
          image   = "rest-portal:1.5"
          name    = "rest-portal"
          command = ["java"]
          args    = ["-jar", "-Djavax.net.debug=ssl:handshake", "app.jar"]
          volume_mount {
            mount_path = "/certs"
            name       = "certs"
          }
          #### Mutual TLS ####
          env {
            name  = "ENABLE_EB_MTLS"
            value = "true"
          }
          env {
            name  = "EB_MTLS_KEYSTORE_PATH"
            value = "/certs/keystore.p12"
          }
          env {
            name  = "EB_MTLS_TRUSTSTORE_PATH"
            value = "/certs/truststore.p12"
          }
          env {
            name = "EB_MTLS_KEYSTORE_PSSWD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret_v1.rest-portal-keystore-password.metadata.0.name
                key  = "password"
              }
            }
          }
          env {
            name  = "EB_MTLS_CERT_PATH"
            value = "/certs/tls.crt"
          }
          env {
            name  = "EB_MTLS_KEY_PATH"
            value = "/certs/tls.key"
          }
          env {
            name  = "EB_MTLS_CA_PATH"
            value = "/certs/ca.crt"
          }

          #### Application Config ####
          env {
            name  = "APP_ZOOKEEPER_HOSTS"
            value = "zookeeper-service"
          }
          env {
            name  = "APP_SERVER_PORT"
            value = "8888"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_service_v1.zookeeper,
    helm_release.kafka,
    kubernetes_manifest.partners-as-certificate
  ]
}