package fr.gabz18.vertlin.mongo

fun mongoSchema(initializer: DbSchemaBuilder.() -> Unit): DbSchemaBuilder {
    return DbSchemaBuilder().apply(initializer)
}

class DbSchemaBuilder() {

    private lateinit var databaseName: String

    private val collections = mutableListOf<CollectionSchemaBuilder>()

    fun collection(collectionName: String, initializer: CollectionSchemaBuilder.() -> Unit) {
        val collection = CollectionSchemaBuilder(collectionName).apply(initializer)
        collections.add(collection)
    }

    fun databaseName(dbName: String) {
        this.databaseName = dbName
    }

    internal fun build() = DbSchema(databaseName, collections.map { it.build() })
}

internal data class DbSchema(val databaseName: String, val collections: Collection<CollectionSchema>)

class CollectionSchemaBuilder(private val collectionName: String) {

    private val indexes = mutableListOf<IndexSchemaBuilder>()

    fun index(name: String, initializer: IndexSchemaBuilder.() -> Unit) {
        val index = IndexSchemaBuilder(name).apply(initializer)
        indexes.add(index)
    }

    internal fun build() = CollectionSchema(collectionName, indexes.map { it.build() })

}

internal data class CollectionSchema(val collectionName: String, val indexes: Collection<IndexSchema>)

class IndexSchemaBuilder(private val name: String) {

    private var unique = false
    private val fields = mutableListOf<IndexFieldSchemaBuilder>()

    fun field(initializer: IndexFieldSchemaBuilder.() -> Unit) {
        val field = IndexFieldSchemaBuilder().apply(initializer)
        fields.add(field)
    }

    fun unique(boolean: Boolean) {
        this.unique = boolean
    }

    internal fun build() = IndexSchema(fields.map { it.build() }, unique)

}

internal data class IndexSchema(val fields: Collection<IndexFieldSchema>, val isUnique: Boolean)

class IndexFieldSchemaBuilder() {

    private lateinit var field: String

    private var isAscending: Boolean = false

    infix fun String.isAscending(isAscending: Boolean) {
        field = this
        this@IndexFieldSchemaBuilder.isAscending = isAscending
    }

    internal fun build() = IndexFieldSchema(field, isAscending)

}

internal data class IndexFieldSchema(val field: String, val isAscending: Boolean)