package fr.gabz18.vertlin.mongo

import fr.gabz18.vertlin.commons.JsonMapper
import fr.gabz18.vertlin.commons.Page
import fr.gabz18.vertlin.commons.PagingSpecification
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.impl.VertxInternal
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import org.bson.json.JsonObject
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

internal fun <T> Publisher<T>.asSingleResultFuture(vertx: VertxInternal): Future<T> {
    val promise = vertx.promise<T>()
    var value: T? = null
    this.subscribe(object : Subscriber<T> {
        override fun onSubscribe(p0: Subscription?) {
            p0?.request(Long.MAX_VALUE)
        }

        override fun onError(err: Throwable?) {
            promise.fail(err)
        }

        override fun onComplete() {
            promise.complete(value)
        }

        override fun onNext(result: T) {
            value = result
        }
    })
    return promise.future()
}

internal fun <T> Publisher<T>.asMultipleResultsFuture(vertx: VertxInternal): Future<Collection<T>> {
    val promise = vertx.promise<Collection<T>>()
    val value = mutableListOf<T>()
    this.subscribe(object : Subscriber<T> {
        override fun onSubscribe(p0: Subscription?) {
            p0?.request(Long.MAX_VALUE)
        }

        override fun onError(err: Throwable?) {
            promise.fail(err)
        }

        override fun onComplete() {
            promise.complete(value)
        }

        override fun onNext(result: T) {
            value.add(result)
        }
    })
    return promise.future()
}

internal fun <T> Publisher<T>.asIteratorCompletionFuture(
    vertx: VertxInternal,
    fn: (T) -> Future<Any>
): Future<Unit> {
    val promise = vertx.promise<Unit>()
    val value = mutableListOf<Future<Any>>()
    this.subscribe(object : Subscriber<T> {
        override fun onSubscribe(p0: Subscription?) {
            p0?.request(Long.MAX_VALUE)
        }

        override fun onError(err: Throwable?) {
            promise.fail(err)
        }

        override fun onComplete() {
            promise.complete()
        }

        override fun onNext(result: T) {
            value.add(fn.invoke(result))
        }
    })
    return CompositeFuture.all(value)
        .flatMap { promise.future() }
}

internal fun <T> Publisher<JsonObject>.asPageFuture(
    vertx: VertxInternal,
    serializer: KSerializer<T>,
    pagingSpecification: PagingSpecification
): Future<Page<T>> {
    val promise = vertx.promise<Page<T>>()

    var page: Page<T>? = null

    this.subscribe(object : Subscriber<JsonObject> {
        override fun onSubscribe(p0: Subscription?) {
            p0?.request(Long.MAX_VALUE)
        }

        override fun onError(err: Throwable?) {
            promise.fail(err)
        }

        override fun onComplete() {
            if (page == null) {
                page = pagingSpecification.toPage(0, listOf())
            }
            promise.complete(page)
        }

        override fun onNext(result: JsonObject) {
            val aggregateResult = JsonMapper.fromJsonString(PagingAggregateResult.serializer(serializer), result.json)
            page = pagingSpecification.toPage(aggregateResult.totalCount.firstOrNull()?.totalCount ?: 0, aggregateResult.data)
        }
    })

    return promise.future()
}

@Serializable
private data class PagingAggregateResult<T>(
    val data: Collection<T>,
    val totalCount: Collection<TotalCount>
)

@Serializable
private data class TotalCount(val totalCount: Long)
