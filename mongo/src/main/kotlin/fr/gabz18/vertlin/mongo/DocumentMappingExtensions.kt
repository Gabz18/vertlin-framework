package fr.gabz18.vertlin.mongo

import fr.gabz18.vertlin.commons.Rfc3339DateSerializer
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDate
import java.time.ZoneOffset
import java.util.Date

@Serializable
private data class ObjectId(@SerialName("\$oid") val oid: String)

@Serializable
private data class MongoDate(@SerialName("\$date") @Serializable(with = Rfc3339DateSerializer::class) val date: Date)

object ObjectIdSerializer : KSerializer<String> {

    override val descriptor: SerialDescriptor
        get() = ObjectId.serializer().descriptor

    override fun deserialize(decoder: Decoder): String {
        val objectId = decoder.decodeSerializableValue(ObjectId.serializer())
        return objectId.oid
    }

    override fun serialize(encoder: Encoder, value: String) {
        val objectId = ObjectId(value)
        encoder.encodeSerializableValue(ObjectId.serializer(), objectId)
    }

}

object MongoDateSerializer : KSerializer<Date> {

    override val descriptor: SerialDescriptor
        get() = MongoDate.serializer().descriptor

    override fun deserialize(decoder: Decoder): Date {
        val mongoDate = decoder.decodeSerializableValue(MongoDate.serializer())
        return mongoDate.date
    }

    override fun serialize(encoder: Encoder, value: Date) {
        val mongoDate = MongoDate(value)
        encoder.encodeSerializableValue(MongoDate.serializer(), mongoDate)
    }

}

object MongoLocalDateSerializer : KSerializer<LocalDate> {

    override val descriptor: SerialDescriptor = MongoDate.serializer().descriptor
    override fun serialize(encoder: Encoder, value: LocalDate) {
        val zonedDateTime = value.atStartOfDay(ZoneOffset.UTC)
        val date = Date.from(zonedDateTime.toInstant())
        val mongoDate = MongoDate(date)
        encoder.encodeSerializableValue(MongoDate.serializer(), mongoDate)
    }

    override fun deserialize(decoder: Decoder): LocalDate {
        val mongoDate = decoder.decodeSerializableValue(MongoDate.serializer())
        return mongoDate.date.toInstant().atZone(ZoneOffset.UTC).toLocalDate()
    }
}