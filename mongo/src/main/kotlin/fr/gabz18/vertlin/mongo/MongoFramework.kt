package fr.gabz18.vertlin.mongo

import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.ReplaceOptions
import com.mongodb.client.result.UpdateResult
import com.mongodb.reactivestreams.client.ClientSession
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import fr.gabz18.vertlin.commons.Logger
import fr.gabz18.vertlin.commons.Page
import fr.gabz18.vertlin.commons.PagingSpecification
import fr.gabz18.vertlin.commons.formatToRfc3339Utc
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.impl.VertxInternal
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.json.JsonObject
import org.bson.types.ObjectId
import java.util.*

@Serializable
abstract class Entity(
    @Serializable(with = ObjectIdSerializer::class) @SerialName("_id") var id: String? = null,
    @Serializable(with = MongoDateSerializer::class) var creationDate: Date? = null,
    @Serializable(with = MongoDateSerializer::class) var modificationDate: Date? = null
)

abstract class MongoRepository<T : Entity, R : MongoRepository<T, R>>(
    mongoClient: MongoClient,
    private val mongoDatabase: MongoDatabase,
    vertx: Vertx,
    private val collectionName: String,
    isTransactional: Boolean = false,
    private val entitySerializer: KSerializer<T>
) {

    private val mongoCollection = mongoDatabase.getCollection(collectionName, JsonObject::class.java)
    private val vertx = vertx as VertxInternal
    private val transactionHolder = if (isTransactional) TransactionHolder(vertx, mongoClient) else null

    private val logger = Logger.logger { }

    protected companion object {
        const val ID_FIELD = "_id"
        const val CREATION_DATE_FIELD = "creationDate"
        const val MODIFICATION_DATE_FIELD = "modificationDate"

        const val DATE_KEYWORD = "\$date"
        const val REGEX_KEYWORD = "\$regex"
        const val OPTIONS_KEYWORD = "\$options"
        const val REGEX_IGNORE_CASE_FORMAT = "i"
    }

    fun <U> transactional(execution: (transactionClient: R) -> Future<U>): Future<U> {
        if (transactionHolder != null) {
            throw Exception("Attempted to start an embedded transaction")
        }
        val transactionClient = cloneForTransaction()
        if (transactionClient.transactionHolder == null) {
            throw Exception("Invalid \"cloneForTransaction()\" implementation, isTransactional should be true")
        }
        return try {
            execution.invoke(transactionClient)
                .recover { err ->
                    transactionClient.transactionHolder.abort().flatMap { Future.failedFuture(err) }
                }
                .flatMap { asyncResult ->
                    transactionClient.transactionHolder.commit().map { asyncResult }
                }
        } catch (err: Exception) {
            transactionClient.transactionHolder.abort().flatMap { Future.failedFuture(err) }
        }
    }

    fun save(entity: T): Future<T> {
        return if (entity.id == null) insertOne(entity) else replaceOne(entity)
    }

    fun findById(id: String): Future<T?> {
        val filter = Document().append(ID_FIELD, ObjectId(id))
        return findOne(filter)
    }

    fun findAndDeleteById(id: String): Future<T?> {
        val filter = Document().append(ID_FIELD, ObjectId(id))
        return findAndDeleteOne(filter)
    }

    protected fun findOne(filter: Bson): Future<T?> {
        return mongoCollection.find(filter).first().asSingleResultFuture(vertx)
            .recover { err ->
                logger.error(err) { "An error occurred while retrieving single document" }
                Future.failedFuture(err)
            }
            .map { document -> document?.mapToEntity() }
    }

    protected fun findMany(filter: Bson): Future<Collection<T>> {
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.find(clientSession, filter).asMultipleResultsFuture(vertx)
        } ?: mongoCollection.find(filter).asMultipleResultsFuture(vertx)
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while retrieving many documents" }
                Future.failedFuture(err)
            }
            .map { documents -> documents.map { it.mapToEntity() } }
    }

    protected fun iterate(filter: Bson, fn: (T) -> Future<Unit>): Future<Unit> {
        fun iteratorExecution(document: JsonObject): Future<Any> = fn.invoke(document.mapToEntity()).mapEmpty()
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.find(clientSession, filter).asIteratorCompletionFuture(vertx) { iteratorExecution(it) }
        } ?: mongoCollection.find(filter).asIteratorCompletionFuture(vertx) { iteratorExecution(it) }
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while iterating over documents" }
                Future.failedFuture(err)
            }
    }

    protected fun findPage(filters: Bson, pagingSpecification: PagingSpecification): Future<Page<T>> {
        val skip =
            Document().append("\$skip", pagingSpecification.pageSize * (pagingSpecification.pageNumber.value - 1))
        val limit = Document().append("\$limit", pagingSpecification.pageSize)

        val matchStage = Document().append("\$match", filters.toBsonDocument())

        val facetStage = Document()
            .append(
                "\$facet", Document()
                    .append("data", listOf(skip, limit))
                    .append("totalCount", listOf(Document().append("\$count", "totalCount")))
            )
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.aggregate(clientSession, listOf(matchStage, facetStage))
                .asPageFuture(vertx, entitySerializer, pagingSpecification)
        } ?: mongoCollection.aggregate(listOf(matchStage, facetStage))
            .asPageFuture(vertx, entitySerializer, pagingSpecification)

        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while retrieving many documents" }
                Future.failedFuture(err)
            }
    }

    protected fun findAndDeleteOne(filters: Bson): Future<T?> {
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.findOneAndDelete(clientSession, filters).asSingleResultFuture(vertx)
        } ?: mongoCollection.findOneAndDelete(filters).asSingleResultFuture(vertx)
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while deleting document" }
                Future.failedFuture(err)
            }
            .map { document ->
                document?.mapToEntity()
            }
    }

    protected fun deleteOne(filter: Document): Future<Boolean> {
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.deleteOne(clientSession, filter).asSingleResultFuture(vertx)
        } ?: mongoCollection.deleteOne(filter).asSingleResultFuture(vertx)
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while deleting document" }
                Future.failedFuture(err)
            }
            .map { deleteResult ->
                deleteResult.deletedCount == 1L
            }
    }

    // TODO pass required args as parameters
    protected abstract fun cloneForTransaction(): R

    private fun insertOne(entity: T): Future<T> {
        val currentDate = Date()
        val objectId = ObjectId(currentDate)
        entity.creationDate = currentDate
        entity.modificationDate = currentDate
        entity.id = objectId.toHexString()
        val document = entity.mapToJson()
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.insertOne(clientSession, document).asSingleResultFuture(vertx)
        } ?: mongoCollection.insertOne(document).asSingleResultFuture(vertx)
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while inserting document" }
                entity.creationDate = null
                entity.modificationDate = null
                entity.id = null
                Future.failedFuture(err)
            }
            .map(entity)
    }

    private fun replaceOne(entity: T): Future<T> {
        val currentDate = Date()
        val objectId = ObjectId(entity.id)
        val modificationDateSnapshot = entity.modificationDate
        entity.modificationDate = currentDate
        val execution = transactionHolder?.execute { clientSession ->
            mongoCollection.replaceById(objectId, entity.mapToJson(), clientSession)
        } ?: mongoCollection.replaceById(objectId, entity.mapToJson())
        return execution
            .recover { err ->
                logger.error(err) { "An error occurred while replacing document" }
                entity.modificationDate = modificationDateSnapshot
                Future.failedFuture(err)
            }
            .map { updateResult ->
                if (updateResult.modifiedCount != 1L) {
                    entity.modificationDate = modificationDateSnapshot
                }
                entity
            }
    }

    protected fun Document.appendOr(vararg filters: Document): Document = this.append("\$or", listOf(*filters))
    protected fun Document.appendIn(key: String, values: Any): Document = this.append(key, Document("\$in", values))
    protected fun Document.appendAnd(vararg filters: Document): Document = this.append("\$and", listOf(*filters))
    protected fun Document.appendGte(key: String, value: Any): Document = this.append(key, Document("\$gte", value))
    protected fun Document.appendFieldExists(key: String, exists: Boolean): Document =
        this.append(key, Document("\$exists", exists))

    protected fun Document.appendGt(key: String, value: Any): Document = this.append(key, Document("\$gt", value))
    protected fun Document.appendLte(key: String, value: Any): Document = this.append(key, Document("\$lte", value))
    protected fun Document.appendLt(key: String, value: Any): Document = this.append(key, Document("\$lt", value))
    protected fun Document.containsIgnoreCase(key: String, value: Any): Document =
        this.append(key, Document(REGEX_KEYWORD, value).append(OPTIONS_KEYWORD, REGEX_IGNORE_CASE_FORMAT))

    protected fun Document.appendDate(key: String, date: Date?): Document = if (date != null)
        this.append(key, Document(DATE_KEYWORD, date.formatToRfc3339Utc())) else this

    protected fun <M> Document.getArray(key: String, fn: (Document) -> M) =
        this.getList(key, Document::class.java).map { fn.invoke(it) }

    private class TransactionHolder(vertx: Vertx, private val mongoClient: MongoClient) {

        private val vertx = vertx as VertxInternal
        private var clientSession: ClientSession? = null

        fun <T> execute(fn: (ClientSession) -> Future<T>): Future<T> {
            if (clientSession != null) {
                return fn.invoke(clientSession!!)
            }

            return mongoClient.startSession().asSingleResultFuture(vertx)
                .flatMap {
                    clientSession = it
                    it.startTransaction()
                    fn.invoke(it)
                }
        }

        fun abort(): Future<Void> {
            if (clientSession == null) {
                throw RuntimeException("Attempted to abort a non existing transaction")
            }
            return clientSession!!.abortTransaction().asSingleResultFuture(vertx)
        }

        fun commit(): Future<Void> {
            if (clientSession == null) {
                throw RuntimeException("Attempted to commit a non existing transaction")
            }
            return clientSession!!.commitTransaction().asSingleResultFuture(vertx)
        }
    }

    private fun MongoCollection<JsonObject>.replaceById(
        objectId: ObjectId,
        document: JsonObject
    ): Future<UpdateResult> {
        val options = ReplaceOptions().upsert(true)
        return this.replaceOne(eq(ID_FIELD, objectId), document, options).asSingleResultFuture(vertx)
    }

    private fun MongoCollection<JsonObject>.replaceById(
        objectId: ObjectId,
        document: JsonObject,
        clientSession: ClientSession
    ): Future<UpdateResult> {
        val options = ReplaceOptions().upsert(true)
        return this.replaceOne(clientSession, Document().append(ID_FIELD, objectId), document, options)
            .asSingleResultFuture(vertx)
    }

    private fun T.mapToJson(): JsonObject {
        val json = Json.encodeToString(entitySerializer, this)
        return JsonObject(json)
    }

    private fun JsonObject.mapToEntity(): T {
        return Json.decodeFromString(entitySerializer, this.json)
    }
}
