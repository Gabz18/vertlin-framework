package fr.gabz18.vertlin.web

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext

abstract class HttpRouter(protected val vertx: Vertx) {

    companion object {
        const val CONTENT_TYPE = "Content-Type"
        const val APPLICATION_JSON = "application/json"
    }

    abstract fun mountRoutes(router: Router)

    protected fun <T> Future<T>.propagateFailure(routingContext: RoutingContext): Future<T> {
        return this
            .onFailure { err -> routingContext.fail(err) }
    }

}