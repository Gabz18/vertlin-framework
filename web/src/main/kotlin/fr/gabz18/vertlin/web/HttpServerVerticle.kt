package fr.gabz18.vertlin.web

import fr.gabz18.vertlin.commons.*
import io.vertx.core.Future
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.HttpException
import io.vertx.ext.web.handler.StaticHandler

abstract class HttpServerVerticle(verticleName: String, logger: Logger, private val serverPort: Int) :
    VertlinVerticle(verticleName, logger) {

    companion object {
        private const val X_FORWARDER_FOR = "X-Forwarded-For"
        private const val INBOUND_LOG_FORMAT = "HTTP Ingress : %s %s - %s"
        private const val OUTBOUND_LOG_FORMAT = "HTTP Response : %d - %dms"
    }

    override fun init(): Future<Void> {

        val bodyHandler = BodyHandler.create()
        val router = Router.router(vertx)

        router.post().handler(bodyHandler)
        router.put().handler(bodyHandler)
        router.patch().handler(bodyHandler)

        val corsHandler = getCorsHandler()
        if (corsHandler != null) {
            router.route().handler(corsHandler)
        }

        router.route()
            .correlationIdLoggerHandler()
            .failureHandler(this::handleFailure)

        configure(router)

        if (enableOptionsEndpoints()) {
            router.routes.groupBy { it.path }.forEach { route ->
                val path = route.key
                val methods = route.value.map { it.methods() }.distinct()
                var allowHeader = ""
                methods.filterNotNull().flatten().forEach { method -> allowHeader += "$method, " }
                if (path == null) {
                    router.options().handler {
                        it.response().putHeader("Allow", "$allowHeader, OPTIONS").end()
                    }
                } else {
                    router.options(path).handler {
                        it.response().putHeader("Allow", "$allowHeader, OPTIONS").end()
                    }
                }
            }
        }

        return vertx
            .createHttpServer()
            .requestHandler(router)
            .listen(serverPort)
            .onComplete { http ->
                if (http.succeeded()) {
                    logger.info { "HTTP server started on port $serverPort" }
                } else {
                    logger.error(http.cause()) { "An error occurred while starting Http Server" }
                }
            }
            .mapEmpty()
    }

    protected abstract fun configure(router: Router)

    protected abstract fun getCorsHandler() : CorsHandler?

    protected abstract fun enableOptionsEndpoints() : Boolean

    protected fun Router.mountRoutes(httpRouter: HttpRouter): Router {
        httpRouter.mountRoutes(this)
        return this
    }

    protected fun Router.configureSinglePageApplicationHandler(rootFolder: String = "webroot") {
        val staticHandler = StaticHandler.create()
        this.getWithRegex("^(?!/api).+\$").handler(staticHandler)
        this.errorHandler(404) {
            if (it.request().uri().startsWith("/api")) {
                it.response().setStatusCode(404).end()
            } else {
                it.reroute("/")
            }
        }
    }

    private fun Route.correlationIdLoggerHandler(): Route {
        this.handler {
            initializeCorrelationId()
            val time = System.currentTimeMillis()
            val remoteClient = getClientAddress(it)
            val method = it.request().method()
            val uri = it.request().uri()
            logger.info { INBOUND_LOG_FORMAT.format(method, uri, remoteClient) }
            it.addEndHandler { _ ->
                val statusCode = it.response().statusCode
                val processingTime = System.currentTimeMillis() - time
                if (statusCode >= 500) {
                    logger.error { OUTBOUND_LOG_FORMAT.format(statusCode, processingTime) }
                } else {
                    logger.info { OUTBOUND_LOG_FORMAT.format(statusCode, processingTime) }
                }
            }
            it.next()
        }
        return this
    }

    private fun handleFailure(routingContext: RoutingContext) {
        val initialCause = routingContext.failure()
        val code: Int
        val reason: String?

        when (initialCause) {
            is ApplicationException -> {
                when (initialCause.type) {
                    ExceptionType.INVALID_DOMAIN_STATE -> {
                        code = 400
                        reason = initialCause.message
                    }

                    ExceptionType.TIMEOUT -> {
                        code = 504
                        reason = initialCause.message
                    }

                    ExceptionType.AGGREGATE_NOT_FOUND -> {
                        code = 404
                        reason = initialCause.message
                    }

                    ExceptionType.REMOTE_SERVICE_UNAVAILABLE -> {
                        code = 502
                        reason = initialCause.message
                    }

                    ExceptionType.UNAUTHORIZED -> {
                        code = 401
                        reason = "Unauthorized"
                    }

                    ExceptionType.UNHANDLED, ExceptionType.TECHNICAL -> {
                        logger.error(initialCause) { "An unhandled exception occurred while handling HTTP request" }
                        code = 500
                        reason = null
                    }
                }
            }

            is HttpException -> {
                code = initialCause.statusCode
                reason = initialCause.message
            }

            else -> {
                logger.error(initialCause) { "An unhandled exception occurred while handling HTTP request" }
                code = 500
                reason = null
            }
        }
        val response = HttpExceptionResponse(
            path = routingContext.request().path(),
            method = routingContext.request().method().name(),
            code = code,
            reason = reason
        )
        routingContext.response()
            .setStatusMessage(response.status)
            .endWithBody(response, response.code)
    }

    private fun getClientAddress(context: RoutingContext): String? {
        val forwardedFor = context.request().getHeader(X_FORWARDER_FOR)
        if (forwardedFor != null && forwardedFor.isNotEmpty()) {
            return forwardedFor
        }
        val inetSocketAddress = context.request().remoteAddress()
        return inetSocketAddress?.host()
    }

}