package fr.gabz18.vertlin.web

import fr.gabz18.vertlin.commons.InvalidDomainStateException
import fr.gabz18.vertlin.commons.JsonMapper
import fr.gabz18.vertlin.commons.PageNumber
import fr.gabz18.vertlin.commons.PagingSpecification
import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.RoutingContext
import kotlinx.serialization.KSerializer

inline fun <reified T> HttpServerResponse.endWithBody(body: T, httpStatusCode: Int = 200) {
    this
        .setStatusCode(httpStatusCode)
        .putHeader(HttpRouter.CONTENT_TYPE, HttpRouter.APPLICATION_JSON)
        .end(JsonMapper.toPrettyString(body))
}

inline fun <reified T> RoutingContext.endWithBody(body: T, httpStatusCode: Int = 200) {
    this
        .response()
        .setStatusCode(httpStatusCode)
        .putHeader(HttpRouter.CONTENT_TYPE, HttpRouter.APPLICATION_JSON)
        .end(JsonMapper.toPrettyString(body))
}

fun RoutingContext.endEmpty() {
    this
        .response()
        .setStatusCode(204)
        .end()
}

fun <T> RoutingContext.bodyAs(serializer: KSerializer<T>): T {
    try {
        val bodyAsString = this.body().asString()
        return JsonMapper.fromJsonString(serializer, bodyAsString)
    } catch (err: Exception) {
        throw InvalidDomainStateException("Invalid request body provided")
    }
}

private const val PAGE_NUMBER = "pageNumber"
private const val PAGE_SIZE = "pageSize"
private const val SORTED_BY = "sortedBy"
private const val SORT_ASCENDING = "sortAsc"

fun RoutingContext.queryParamsToPagingSpecification(): PagingSpecification {
    val pageNumber = this.queryParams()[PAGE_NUMBER]?.toIntOrNull() ?: 1
    val pageSize = this.queryParams()[PAGE_SIZE]?.toIntOrNull() ?: 10
    val sortedBy = this.queryParams()[SORTED_BY] ?: ""
    val isSortAsc = this.queryParams()[SORT_ASCENDING]?.toBooleanStrictOrNull() ?: true

    return PagingSpecification(PageNumber(pageNumber), pageSize, sortedBy, isSortAsc)
}