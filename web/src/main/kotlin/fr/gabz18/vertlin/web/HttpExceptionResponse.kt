package fr.gabz18.vertlin.web

import fr.gabz18.vertlin.commons.Rfc3339DateSerializer
import kotlinx.serialization.Serializable
import java.util.Date

@Serializable
data class HttpExceptionResponse(
    val path: String,
    val method: String,
    val code: Int,
    val reason: String?,
    @Serializable(Rfc3339DateSerializer::class) val timestamp: Date = Date()
) {
    val status = when (code) {
        400 -> "Bad Request"
        401 -> "Unauthorized"
        403 -> "Forbidden"
        404 -> "Not Found"
        500 -> "Internal Server Error"
        501 -> "Not Implemented"
        502 -> "Bad Gateway"
        503 -> "Service Unavailable"
        504 -> "Gateway Timeout"
        else -> ""
    }

}
