package fr.gabz18.vertlin.commons

import mu.KLogger
import mu.KotlinLogging

class Logger private constructor(private val wrappee: KLogger) {

    companion object {
        private const val FULL_FORMAT = "/%s -> %s"
        private const val TINY_FORMAT = " -> %s"

        fun logger(func: () -> Unit): Logger {
            return Logger(KotlinLogging.logger(func))
        }
    }

    fun debug(func: () -> String) {
        wrappee.debug {
            buildLogMessage(func)
        }
    }

    fun info(func: () -> String) {
        wrappee.info {
            buildLogMessage(func)
        }
    }

    fun warn(func: () -> String) {
        wrappee.warn {
            buildLogMessage(func)
        }
    }

    fun error(func: () -> String) {
        wrappee.error {
            buildLogMessage(func)
        }
    }

    fun error(throwable: Throwable, func: () -> String) {
        wrappee.error(throwable) {
            buildLogMessage(func)
        }
    }

    private fun buildLogMessage(func: () -> String): String {
        val correlationId = getCorrelationId()
        return if (correlationId != null) {
            FULL_FORMAT.format(correlationId, func.invoke())
        } else {
            TINY_FORMAT.format(func.invoke())
        }
    }
}
