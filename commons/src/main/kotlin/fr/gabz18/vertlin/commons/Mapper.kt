package fr.gabz18.vertlin.commons

interface Mapper<IN, OUT> {

    fun map(value: IN): OUT

    fun map(value: Collection<IN>): Collection<OUT> {
        return value.map { map(it) }
    }

    fun map(value: Page<IN>) : Page<OUT> {
        return value.mapContent { map(it) }
    }

}