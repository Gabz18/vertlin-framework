package fr.gabz18.vertlin.commons

import io.vertx.core.Vertx
import java.util.UUID

private const val VERTICLE_TITLE = "verticle-title"

private const val CORRELATION_ID = "correlation-id"

internal fun initializeVerticleTitle(title: String) {
    Vertx.currentContext()?.put(VERTICLE_TITLE, title)
}

internal fun getVerticleTitle(): String? {
    return Vertx.currentContext()?.get<String>(VERTICLE_TITLE)
}

fun initializeCorrelationId() {
    val context = Vertx.currentContext();
    context.putLocal(CORRELATION_ID, UUID.randomUUID().toString())
}

fun setCorrelationId(correlationId: String?) {
    if (correlationId != null) {
        val context = Vertx.currentContext();
        context.putLocal(CORRELATION_ID, correlationId)
    }
}

fun getCorrelationId(): String? {
    return Vertx.currentContext()?.getLocal<String?>(CORRELATION_ID)
}

fun getOrGenerateCorrelationId(): String {
    val context = Vertx.currentContext();
    val correlationId = context.getLocal<String?>(CORRELATION_ID)
    return if (correlationId == null) {
        val generatedId = UUID.randomUUID().toString()
        context.putLocal(CORRELATION_ID, generatedId)
        generatedId
    } else {
        correlationId
    }
}
