package fr.gabz18.vertlin.commons

import kotlinx.serialization.Serializable

@Serializable
data class Page<T>(
    val pageNumber: PageNumber,
    val pageSize: Int = 10,
    val sortedBy: String? = null,
    val sortAscending: Boolean? = false,
    val totalCount: Long,
    val content: Collection<T>
) {

    fun <U> mapContent(fn: (T) -> U): Page<U> {
        return Page(
            pageNumber, pageSize, sortedBy, sortAscending, totalCount, content.map { fn.invoke(it) }
        )
    }

}

@Serializable
data class PagingSpecification(
    val pageNumber: PageNumber,
    val pageSize: Int,
    val sortedBy: String,
    val sortAscending: Boolean
) {

    fun <T> toPage(totalCount: Long, content: Collection<T>): Page<T> {
        return Page(
            this.pageNumber,
            this.pageSize,
            this.sortedBy,
            this.sortAscending,
            totalCount,
            content
        )
    }
}

@JvmInline
@Serializable
value class PageNumber(val value: Int) {

    init {
        if (value < 1) {
            throw InvalidDomainStateException("Page number cannot be less than 1")
        }
    }
}