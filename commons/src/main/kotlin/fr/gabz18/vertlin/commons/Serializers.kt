package fr.gabz18.vertlin.commons

import io.vertx.core.buffer.Buffer
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDate
import java.util.*

object DateSerializer : KSerializer<Date> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Date", PrimitiveKind.LONG)
    override fun serialize(encoder: Encoder, value: Date) = encoder.encodeLong(value.time)
    override fun deserialize(decoder: Decoder): Date = Date(decoder.decodeLong())
}

object Rfc3339DateSerializer : KSerializer<Date> {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("Date", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Date {
        return decoder.decodeString().toRfc3339UtcDate()
    }

    override fun serialize(encoder: Encoder, value: Date) {
        encoder.encodeString(value.formatToRfc3339Utc())
    }

}

object LocalDateSerializer : KSerializer<LocalDate> {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): LocalDate = LocalDate.parse(decoder.decodeString())

    override fun serialize(encoder: Encoder, value: LocalDate) {
        encoder.encodeString(value.toString())
    }
}

object BufferSerializer : KSerializer<Buffer> {

    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("Buffer", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Buffer {
        val bufferAsString = decoder.decodeString()
        return Buffer.buffer(bufferAsString.toByteArray())
    }

    override fun serialize(encoder: Encoder, value: Buffer) {
        val bufferAsString = String(value.bytes)
        encoder.encodeString(bufferAsString)
    }

}