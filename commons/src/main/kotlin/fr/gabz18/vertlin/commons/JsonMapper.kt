package fr.gabz18.vertlin.commons

import kotlinx.serialization.KSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

object JsonMapper {

    val standardMapper = Json {
        isLenient = true
        coerceInputValues = true
        ignoreUnknownKeys = true
        encodeDefaults = true
    }

    val prettyMapper = Json {
        encodeDefaults = true
        prettyPrint = true
    }

    fun <T> fromJsonString(serializer: KSerializer<T>, input: String) : T {
        return standardMapper.decodeFromString(serializer, input)
    }

    inline fun <reified T> fromJsonString(input: String) : T {
        return standardMapper.decodeFromString(input)
    }

    fun <T> toJsonString(serializer: KSerializer<T>, input: T) : String {
        return standardMapper.encodeToString(serializer, input)
    }

    inline fun <reified T> toString(value: T) : String {
        return standardMapper.encodeToString(value)
    }

    fun <T> toPrettyJsonString(serializer: KSerializer<T>, input: T) : String {
        return prettyMapper.encodeToString(serializer, input)
    }

    inline fun <reified T> toPrettyString(value: T) : String {
        return prettyMapper.encodeToString(value)
    }

}
