package fr.gabz18.vertlin.commons

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Promise

abstract class VertlinVerticle(private val verticleTitle: String, protected val logger: Logger) : AbstractVerticle() {

    final override fun start(promise: Promise<Void>) {
        val startingTime = System.currentTimeMillis()
        initializeVerticleTitle(verticleTitle)
        init()
            .onComplete {
                if (it.failed()) {
                    logger.error(it.cause()) { "An error occurred while deploying $verticleTitle" }
                    promise.fail(it.cause())
                } else {
                    logger.info { "Successfully deployed $verticleTitle in ${System.currentTimeMillis() - startingTime} milliseconds" }
                    promise.complete()
                }
            }
    }

    abstract fun init(): Future<Void>

}