package fr.gabz18.vertlin.commons

private val logger = Logger.logger { }

/**
 * Categories of exceptions that may occur in the application lifespan
 */
enum class ExceptionType(val code: Int) {

    UNHANDLED(1),
    INVALID_DOMAIN_STATE(2),
    AGGREGATE_NOT_FOUND(3),
    REMOTE_SERVICE_UNAVAILABLE(4),
    TIMEOUT(5),
    TECHNICAL(6),
    UNAUTHORIZED(7);

    companion object {

        fun fromInt(code: Int): ExceptionType {
            return when (code) {
                1 -> UNHANDLED
                2 -> INVALID_DOMAIN_STATE
                3 -> AGGREGATE_NOT_FOUND
                4 -> REMOTE_SERVICE_UNAVAILABLE
                5 -> TIMEOUT
                6 -> TECHNICAL
                7 -> UNAUTHORIZED
                else -> TECHNICAL
            }
        }
    }
}

/**
 * Top class of every application managed exception
 */
abstract class VertlinException(val type: ExceptionType, message: String?) : Exception(message)

/**
 * Represents an exception related to business and domain expectations failures
 */
open class ApplicationException(type: ExceptionType, message: String?) : VertlinException(type, message) {

    init {
        if (type == ExceptionType.REMOTE_SERVICE_UNAVAILABLE || type == ExceptionType.TIMEOUT) {
            logger.warn { "ServiceException was built with unadapted type : $type" }
        }
    }

}

/**
 * Represents an exception related to inter process communication technical exceptions
 */
class CommunicationException(type: ExceptionType, message: String?) : VertlinException(type, message) {

    init {
        if (type == ExceptionType.INVALID_DOMAIN_STATE || type == ExceptionType.AGGREGATE_NOT_FOUND) {
            logger.warn { "CommunicationException was built with unadapted type : $type" }
        }
    }

}

class InvalidDomainStateException(message: String) : ApplicationException(ExceptionType.INVALID_DOMAIN_STATE, message)

class AggregateNotFoundException(message: String) : ApplicationException(ExceptionType.AGGREGATE_NOT_FOUND, message)

class TechnicalException(message: String?) : VertlinException(ExceptionType.TECHNICAL, message)

class UnauthorizedException() : VertlinException(ExceptionType.UNAUTHORIZED, null)