package fr.gabz18.vertlin.commons

import io.vertx.core.Future

fun <T> Future<T?>.failIfEmpty(fn: () -> Throwable): Future<T> {
    return this.map {
        if (it == null) throw fn.invoke()
        return@map it
    }
}

fun <T> Future<T?>.failIfNonEmpty(fn: () -> Throwable): Future<Unit> {
    return this.map {
        if (it != null) throw fn.invoke()
    }
}