package fr.gabz18.vertlin.commons

import java.text.SimpleDateFormat
import java.util.*

private object DateFormat {

    private val wrapped = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private val wrapped2 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    init {
        wrapped.timeZone = TimeZone.getTimeZone("UTC")
    }

    fun parse(value: String): Date {
        return try {
            wrapped.parse(value)
        } catch (e: Exception) {
            wrapped2.parse(value)
        }
    }

    fun format(value: Date): String {
        return wrapped.format(value)
    }

}

fun Date.formatToRfc3339Utc(): String = DateFormat.format(this)

fun String.toRfc3339UtcDate(): Date = DateFormat.parse(this)